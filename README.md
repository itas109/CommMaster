# CommMaster

## 介绍
跨平台串口通信助手-通信大师CommMaster

## 软件架构
* windows x86
* windows x86_64
* linux x86_64
* macos x86_64
* raspberrypi armhf

## 安装教程

### 1.windows
#### 1.1 xp 
安装CommMaster-x86-1.0.0.zip

#### 1.2 win7及以上
安装CommMaster-x86-1.0.0.exe

### 2.linux
#### 2.1 Debian/Ubuntu
```
sudo dpkg -i CommMaster-x86_64-1.0.0.deb
```

#### 2.2 其他
```
./CommMaster-x86_64-1.0.0.run
```

### 3.macos
```
CommMaster-x86_64-1.0.0.dmg
```

### 4.raspberrypi
```
sudo dpkg -i CommMaster-armhf-1.0.0.deb
```

## 使用说明

![image](./pic/commMaster.png)

## 其他

本软件基于CSerialPort开发

发布地址：https://gitee.com/itas109/CommMaster

https://github.com/itas109/CSerialPort

https://blog.csdn.net/itas109

QQ群:129518033
