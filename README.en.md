# CommMaster

## Description
跨平台串口通信助手-通信大师CommMaster

## Software Architecture
* windows x86
* windows x86_64
* linux x86_64
* macos x86_64
* raspberrypi armhf

## Installation


## Instructions

### 1.windows
#### 1.1 xp 
install CommMaster-x86-1.0.0.zip

#### 1.2 win7 and above
install CommMaster-x86-1.0.0.exe

### 2.linux
#### 2.1 Debian/Ubuntu
```
sudo dpkg -i CommMaster-x86_64-1.0.0.deb
```

#### 2.2 other
```
./CommMaster-x86_64-1.0.0.run
```

### 3.macos
```
CommMaster-x86_64-1.0.0.dmg
```

### 4.raspberrypi
```
sudo dpkg -i CommMaster-armhf-1.0.0.deb
```

## Usage

![image](./pic/commMaster.png)

## Other

CommMaster based CSerialPort

Release URL：https://gitee.com/itas109/CommMaster

https://github.com/itas109/CSerialPort
https://blog.csdn.net/itas109
QQ Group:129518033
